# ActivityPub Server in a Single PHP File

This is a single PHP file - and an `.htaccess file` - which acts as an extremely basic ActivityPub server.

## Getting started

This is designed to be a lightweight educational tool to show you the basics of how ActivityPub works.

There are no tests, no checks, no security features, no formal verifications, no containers, no gods, no masters.

1. Edit the `index.php` file to add a username, password, and keypair.
1. Upload `index.php` and `.htaccess` to the *root* directory of your domain. For example `test.example.com/`. It will not work in a subdirectory.
1. Optionally, upload an `icon.png` to make the user look nice.
1. Visit `https://test.example.com/.well-known/webfinger` and check that it shows a JSON file with your user's details.
1. Go to Mastodon or other Fediverse site and search for your user: `@username@test.example.com`
1. Follow your user.
1. Check your `/data/logs/` directory to see if the follow request was received correctly.
1. To post a message, visit `https://test.example.com/write` type in your message and password. Press the "Post Message" button.
1. Check social media to see if the message appears.
1. To follow other users, visit `https://test.example.com/follow` type in the name of the user you want to follow and your password. Press the "Send Follow Request" button.
1. Check social media to see if the follow request came through.
1. To read the messages that your server's inbox has received, visit `https://test.example.com/read`

## How this works

* The `.htaccess` file transforms requests from `example.com/whatever` to `example.com/index.php?path=whatever`.
* The `index.php` file performs a specific action depending on the path requested.
* Log files are saved as .txt in the `/data/logs` directory.
* Post files are saved as .json in the `/posts` directory.
* Details of accounts who follow you are saved as .json in the `/data/followers` directory.
* Details of accounts who you follow are saved as .json in the `/data/following` directory.
* Messages sent to your inbox are saved as .json in the `/data/inbox` directory.
* This has sloppy support for sending posts with linked #hashtags, https:// URls, and @ mentions.
* HTTP Message Signatures are verified.

##	Requirements

* PHP 8.3 (We live in the future now)
* The [OpenSSL Extension](https://www.php.net/manual/en/book.openssl.php) (This is usually installed by default)
* HTTPS certificate (Let's Encrypt is fine)
* 100MB free disk space (ActivityPub is a very "chatty" protocol. Expect lots of logs.)
* Docker, Node, MongoDB, Wayland, GLaDOS, React, LLM, Adobe Creative Cloud, Maven (Absolutely none of these!)

## Licence

This code is released to you under the [GNU Affero General Public License v3.0 or later](https://www.gnu.org/licenses/agpl-3.0.html).  This means, if you modify this code and let other people interact with it over a computer network, [you **must** release the source code](https://www.gnu.org/licenses/gpl-faq.html#UnreleasedModsAGPL).

I actively do *not* want you to use this code in production. It is not suitable for anything other than educational use.  The use of AGPL is designed to be an incentive for you to learn from this software and then write something better.

Please take note of [CRAPL v0](https://matt.might.net/articles/crapl/):

> Any appearance of design in the Program is purely coincidental and should not in any way be mistaken for evidence of thoughtful software construction.

## Features

### Working

* ✅ User can be followed
* ✅ User can post messages to followers
* ✅ User can post a message to a specific external account
* ✅ User can post an image & alt text
* ✅ User can post text which is converted into basic HTML (mentions, URls, hashtags)
* ✅ User can follow, unfollow, block, and unblock external accounts
* ✅ Server validates the HTTP Message Signatures of all incoming messages
* ✅ Incoming messages are saved
* ✅ Process Undo / Delete messages (Unfollow, delete post, remove Like, remove Announce)
* ✅ User can read incoming messages (Create, Update, Announce, Like)
* ✅ User can read the results of polls
* ✅ User can see attached images, videos, audio
* ✅ User can hide "Content Warning" text
* ✅ User can receive Updates to messages
* ✅ User can Announce / boost / repost an external post
* ✅ User can see if a post is a reply
* ✅ User can see if a post is a reply to them

### Not Working

* ❌ See posts from Lemmy communities
* ❌ See contents of Announced posts
* ❌ Thread replies in context
* ❌ Delete own post
* ❌ Update own post
* ❌ Create Polls
* ❌ Attach multiple images
* ❌ Set focus point for images
* ❌ Set sensitivity for images / blur
* ❌ Set "Content Warning"
* ❌ See external users' avatars, names, or other details
* ❌ Viewing all posts by user with a specific hashtag
* ❌ Accurate support for converting user's text to HTML
* ❌ ...?
